# SPACE WARS

Shoot enemies and do not let them pass your edge of screen. Enemies will get faster as you gain more score, and tougher enemies may appear. You can pick up a package that will allow you to shoot two shots at once. 

## Controls

Menu – W, S: move up/down, D: select, Q: end 

Choose_color – W, S: move up/down, D: choose color, A: confirm and return 

Game – W,S,A,D: movement, E: shoot 


## Installation
Clone git rep (`git clone git@gitlab.fel.cvut.cz:removale/apo_semestralka.git`) (or simply download it)

Compile using `make TARGET_IP=192.168.202.xx run` (or edit target ip in makefile and just use `make run`) 


