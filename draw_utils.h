#ifndef DRAW_UTILS_H
#define DRAW_UTILS_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"



extern font_descriptor_t* fdes;

void draw_pixel(int x, int y, unsigned short color, unsigned short* fb);

void draw_pixel_big(int x, int y, unsigned short color, int scale, unsigned short* fb);

int char_width(int ch);

void draw_char(int x, int y, char ch, unsigned short color, int scale, unsigned short* fb);

void draw_string(int x, int y, char* string, unsigned short color, int scale, unsigned short* fb);


#endif  /*ENEMY_H*/

