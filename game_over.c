#include "game_over.h"


#ifndef WIDTH
#define WIDTH 480
#endif 

#ifndef HEIGTH
#define HEIGTH 320 
#endif 

#ifndef FONT_SIZE_BIG
#define FONT_SIZE_BIG 3
#endif

#ifndef FONT_SIZE_NORMAL
#define FONT_SIZE_NORMAL 2
#endif

#ifndef FONT_SIZE_SMALL
#define FONT_SIZE_SMALL 1
#endif

unsigned short* fb;

void draw_game_over(unsigned char* parlcd_mem_base, int score) {
	int ptr;
	char score_text[5];
	fb = (unsigned short*)malloc(320 * 480 * 2);

	//score = 125;

	for (ptr = 0; ptr < 320 * 480; ptr++) {
		fb[ptr] = 0;
	}

	draw_string(25, 10, "DO OR DO NOT", hsv2rgb_lcd(120, 255, 255), FONT_SIZE_BIG);
	draw_string(25, 60, "THERE IS NO TRY", hsv2rgb_lcd(120, 255, 255), FONT_SIZE_BIG);

	if (score == 66) {
		draw_string(25, 110, "Execute order 66", hsv2rgb_lcd(0, 255, 255), FONT_SIZE_NORMAL);
	}
	/*else if (score >= 100) {
		draw_string(25, 110, "We want YOU to", hsv2rgb_lcd(0, 255, 255), FONT_SIZE_NORMAL);
		draw_string(25, 150, "fight against rebellion", hsv2rgb_lcd(0, 255, 255), FONT_SIZE_NORMAL);
	}*/


	draw_string(10, 200, "Score: ", hsv2rgb_lcd(0, 0, 255), FONT_SIZE_NORMAL);
	sprintf(score_text, "%d", score);
	draw_string(120, 200, score_text, hsv2rgb_lcd(0, 255, 255), FONT_SIZE_NORMAL);

	parlcd_write_cmd(parlcd_mem_base, 0x2c);
	for (ptr = 0; ptr < 480 * 320; ptr++) {
		parlcd_write_data(parlcd_mem_base, fb[ptr]);
	}
	sleep(5);
	play_anim(parlcd_mem_base);

	
}

void play_anim(unsigned char* parlcd_mem_base) {
	int i, j;
	int ptr;

	struct timespec loop_delay;
	loop_delay.tv_sec = 0;
	loop_delay.tv_nsec = 50 * 1000 * 1000;

	int size_1_y = 8;
	int size_2_y = 8;
	int size_3_x = 8;
	int size_4_x = 8;

	int yy_1 = 0, xx_3 = 0;
	int yy_2 = HEIGTH - size_2_y;
	int xx_4 = WIDTH - size_4_x;
	int mult1 = -1, mult2 = 1, mult3 = -1, mult4 = 1;


	while (true) {
		for (ptr = 0; ptr < 320 * 480; ptr++) {
			fb[ptr] = 0u;
		}

		// Red Line
		for (j = 0; j < size_1_y; j++) {
			for (i = 0; i < WIDTH; i++) {
				draw_pixel(i, j + yy_1, 0xf800); // RED: 1111 1|000 000|0 0000  => F800
			}
		}

		//Blue Line
		for (j = 0; j < size_2_y; j++) {
			for (i = 0; i < WIDTH; i++) {
				draw_pixel(i, j + yy_2, 0x001f); // RED: 0000 0|000 000|1 1111  => 001f
			}
		}

		//Green Line
		for (j = 0; j < HEIGTH; j++) {
			for (i = 0; i < size_3_x; i++) {
				draw_pixel(i + xx_3, j, 0x07e0); // GREEN: 0000 0|111 111|0 0000  => 07e0
			}
		}

		//Yellow Line
		for (j = 0; j < HEIGTH; j++) {
			for (i = 0; i < size_4_x; i++) {
				draw_pixel(i + xx_4, j, 0xffe0); // YELLOW: 1111 1|111 111|0 0000  => ffe0
			}
		}

		if (yy_1 >= (HEIGTH - size_1_y) || yy_1 <= 0) {
			mult1 *= -1;
		}
		if (yy_2 >= (HEIGTH - size_2_y) || yy_2 <= 0) {
			mult2 *= -1;
		}
		if (xx_3 >= (WIDTH - size_3_x) || xx_3 <= 0) {
			mult3 *= -1;
		}
		if (xx_4 >= (WIDTH - size_4_x) || xx_4 <= 0) {
			mult4 *= -1;
		}

		yy_1 = yy_1 + (10 * mult1);
		yy_2 = yy_2 + (10 * mult2);
		xx_3 = xx_3 + (15 * mult3);
		xx_4 = xx_4 + (15 * mult4);

		if (yy_1 == 0) {
			break;
		}


		parlcd_write_cmd(parlcd_mem_base, 0x2c);
		for (ptr = 0; ptr < 480 * 320; ptr++) {
			parlcd_write_data(parlcd_mem_base, fb[ptr]);
		}

		clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
	}
	for (ptr = 0; ptr < 320 * 480; ptr++) {
		fb[ptr] = 0u;
	}

	parlcd_write_cmd(parlcd_mem_base, 0x2c);
	for (ptr = 0; ptr < 480 * 320; ptr++) {
		parlcd_write_data(parlcd_mem_base, 0);
	}
}


