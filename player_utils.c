#include "player_utils.h"

void draw_player(int x, int y, unsigned int color) {
	// parameters for shaping
	int k = 0;
	bool increasing = true;

	for (int j = 0; j < 60; j++) {
		bool add = false;

		for (int i = 0; i < 60; i++) {

			// 2 side wings
			if ((10 < i && i < 15) || (45 < i && i < 50)) {
				draw_pixel(i + x, j + y, color);
			}

			//inside hull 15->45
			if (15 < j && j < 45) {
				add = true;

				//shaping
				if ((((15 + 15) - k) <= i) && (i <= ((45 - 15) + k))) {
					draw_pixel(i + x, j + y, color);
				}
			}
		}

		if (k >= 15) {
			increasing = false;
		}
		if (k <= 0) {
			increasing = true;
		}

		if (add) {
			if (increasing) {
				k++;
			}
			else {
				k--;
			}
		}
	}
}

int get_enemies_from_score(int score) {
	if (score < 5) {
		return rand() % 5 + 1;
	}
	else if (score < 10) {
		return rand() % 10 + 1;
	}
	else if (score < 20) {
		return rand() % 20 + 1;
	}
	else if (score < 30) {
		return rand() % 25 + 1;
	}
	else
	{
		return rand() % 100 + 1;
	}
}

