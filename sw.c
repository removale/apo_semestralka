
/*******************************************************************
  Project SPACE WARS made by Alex Removcik && Tomas Poskocil

  sw.c      - main file

  include your name there and license for distribution.
  license from EPIC GAMES� :)
  
 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

/*#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>
#include <termios.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

#include "enemy.h"
#include "shot.h"
#include "color_utils.h"
#include "player_utils.h"
#include "game_over.h"
#include "game.h"*/
//#include "draw_utils.h"
#include "sw.h"

#define FONT_SIZE_BIG 3
#define FONT_SIZE_NORMAL 2
#define FONT_SIZE_SMALL 1


/*unsigned short* fb;
font_descriptor_t* fdes;*/

void draw_pixel(int x, int y, unsigned short color) {
	if (x >= 0 && x < 480 && y >= 0 && y < 320) {
		fb[x + 480 * y] = color;
	}
}

void draw_pixel_big(int x, int y, unsigned short color, int scale) {
	int i, j;
	for (i = 0; i < scale; i++) {
		for (j = 0; j < scale; j++) {
			draw_pixel(x + i, y + j, color);
		}
	}
}

int char_width(int ch) {
	int width;
	if (!fdes->width) {
		width = fdes->maxwidth;
	}
	else {
		width = fdes->width[ch - fdes->firstchar];
	}
	return width;
}

void draw_char(int x, int y, char ch, unsigned short color, int scale) {
	int w = char_width(ch);
	const font_bits_t* ptr;
	if ((ch >= fdes->firstchar) && (ch - fdes->firstchar < fdes->size)) {
		if (fdes->offset) {
			ptr = &fdes->bits[fdes->offset[ch - fdes->firstchar]];
		}
		else {
			int bw = (fdes->maxwidth + 15) / 16;
			ptr = &fdes->bits[(ch - fdes->firstchar) * bw * fdes->height];
		}
		int i, j;
		for (i = 0; i < fdes->height; i++) {
			font_bits_t val = *ptr;
			for (j = 0; j < w; j++) {
				if ((val & 0x8000) != 0) {
					draw_pixel_big(x + scale * j, y + scale * i, color, scale);
				}
				val <<= 1;
			}
			ptr++;
		}
	}
}

void draw_string(int x, int y, char* string, unsigned short color, int scale) {
	char ch = string[0];
	int i = 0;
	int width = char_width(ch);

	while (ch != '\0') {
		draw_char(x, y, ch, color, scale);
		x += width * scale;
		i++;
		ch = string[i];
	}
}


void main_menu(unsigned char* parlcd_mem_base) {
	int ptr;

	fdes = &font_winFreeSystem14x16;
	fb = (unsigned short*)malloc(320 * 480 * 2);

	int selector = 0;
	char ch1 = ' ';
	bool menu = true;

	unsigned int selected_color = 120;
	unsigned int color = hsv2rgb_lcd(0, 255, 255);
	unsigned int selected_color_lcd = hsv2rgb_lcd(selected_color, 255, 255);

	while (menu) {

		for (ptr = 0; ptr < 320 * 480; ptr++) {
			fb[ptr] = 0;
		}

		draw_string(50, 10, "SPACE WARS", color, FONT_SIZE_BIG);

		draw_string(150, 100, "START", color, FONT_SIZE_NORMAL);

		draw_string(150, 150, "COLOR", color, FONT_SIZE_NORMAL);

		draw_string(150, 200, "EXIT", color, FONT_SIZE_NORMAL);

		int r = read(0, &ch1, 1);
		while (r == 1) {
			if (ch1 == 'w') {
				selector--;
				if (selector < 0) {
					selector = 2;
				}
			}
			else if (ch1 == 's') {
				selector++;
			}
			else if (ch1 == 'd') {
				switch (selector) {
				case 0:
					printf("Game choosen\n");
					game(parlcd_mem_base, selected_color_lcd);
					break;
				case 1:
					selected_color = choose_color(selected_color);
					selected_color_lcd = hsv2rgb_lcd(selected_color, 255, 255);
					break;
				case 2:
					menu = false;
					break;
				default:
					break;
				}
			}
			else if (ch1 == 'q') {
				menu = false;
			}
			r = read(0, &ch1, 1);
		}

		selector = selector % 3;

		printf("Selector: %i\n", selector);

		switch (selector) {
		case 0:
			draw_string(150, 100, "START", selected_color_lcd, FONT_SIZE_NORMAL);
			break;
		case 1:
			draw_string(150, 150, "COLOR", selected_color_lcd, FONT_SIZE_NORMAL);
			break;
		case 2:
			draw_string(150, 200, "EXIT", selected_color_lcd, FONT_SIZE_NORMAL);
			break;
		default:
			break;
		}

		parlcd_write_cmd(parlcd_mem_base, 0x2c);
		for (ptr = 0; ptr < 480 * 320; ptr++) {
			parlcd_write_data(parlcd_mem_base, fb[ptr]);
		}
		sleep(1);
	}
}


int main(int argc, char* argv[]) {
	unsigned char* parlcd_mem_base;

	printf("Hello\n");
	parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
	if (parlcd_mem_base == NULL)
		exit(1);

	parlcd_hx8357_init(parlcd_mem_base);

	static struct termios oldt, newt;

	/*tcgetattr gets the parameters of the current terminal
		STDIN_FILENO will tell tcgetattr that it should write the settings
	   of stdin to oldt*/
	tcgetattr(STDIN_FILENO, &oldt);
	/*now the settings will be copied*/
	newt = oldt;

	/*ICANON normally takes care that one line at a time will be processed
	that means it will return if it sees a "\n" or an EOF or an EOL*/
	newt.c_lflag &= ~(ICANON);
	newt.c_cc[VMIN] = 0; // bytes until read unblocks.
	newt.c_cc[VTIME] = 0;

	/*Those new settings will be set to STDIN
	TCSANOW tells tcsetattr to change attributes immediately. */
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);

	//play_anim(parlcd_mem_base);

	main_menu(parlcd_mem_base);

	printf("Goodbye\n");

	return 0;
}	
