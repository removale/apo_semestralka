#include "enemy.h"


#ifndef UP
#define UP 3
#endif

#ifndef RIGHT
#define RIGHT 2
#endif

#ifndef LEFT
#define LEFT 1
#endif

#ifndef DOWN
#define DOWN 0
#endif

#ifndef WHITE
#define WHITE 0xffff
#endif

#ifndef GREY
#define GREY 0x6306
#endif

#ifndef BLACK
#define BLACK 0x0000
#endif

#ifndef BLUE
#define BLUE 0x0618
#endif

#ifndef ORANGE
#define ORANGE 0xfbe0
#endif


void init_enemies(Enemy** enemies, int number_of_enemies, int make_enemies, int hp) {
	int wave = 0;
	
	for (int e = 0; e < make_enemies; e++) {
		Enemy* enemy = malloc(sizeof(Enemy));
		enemy->x = 10 + (80 * (e % 5));
		enemy->y = -60 - (80 * wave);
		enemy->hp = rand()%hp +1;
		enemies[e] = enemy;
		

		if ((e + 1) % 5 == 0) {
			wave++;
		}
	}
}


void move_en(Enemy* enemy, int dir) {
	switch (dir) {
	case UP: enemy->y -= 20; break;
	case DOWN: enemy->y += 20; break;
	case RIGHT: enemy->x += 50; break;
	case LEFT: enemy->x -= 50; break;
	}
}


void draw_enemy(int x, int y, int hp) {
	// parameters for shaping
	int k = 0;
	bool increasing = true;
	unsigned short color = 0xf800;
	if(hp > 1) color = BLUE;	// 1111 1|110 111|0 0000
	for (int j = 0; j < 60; j++) {
		bool add = false;

		for (int i = 0; i < 60; i++) {

			// End part
			if (j <= 13) {

				if ((j <= 2) && ((17 <= i && i <= 20) || (41 <= i && i <= 44))) {
					draw_pixel(i + x, j + y, ORANGE);
				}
				
				// Engines
				if ( (3 <= j && j <= 13) && ((17 <= i && i <= 20) || (41 <= i && i <= 44)) ) {

					if (3 <= j && j <= 6) {
						draw_pixel(i + x, j + y, GREY); // 0110 0|011 000|0 1100
					}
					else {
						draw_pixel(i + x, j + y, WHITE);
					}					
				}

				// End part of end
				if ( (6 <= j && j <= 11) && ((24 <= i && i <= 28) || (33 <= i && i <= 37)) ) {
					draw_pixel(i + x, j + y, color);
				}

				// Front part of end
				if ((12 <= j && j <= 13) && (24 <= i && i <= 37)) {
					draw_pixel(i + x, j + y, color);
				}
			}
			else if (14 <= j && j <= 25) { // Middle part
				
				if (j == 14 && (14 <= i && i <= 46)) {
					draw_pixel(i + x, j + y, GREY);
				} 
				else if (j == 15 && (10 <= i && i <= 50)) {
					draw_pixel(i + x, j + y, GREY);
				}
				else if (j == 16 && (6 <= i && i <= 54)) {
					draw_pixel(i + x, j + y, WHITE);
				}
				else if (j == 17 && (2 <= i && i <= 58)) {
					draw_pixel(i + x, j + y, WHITE);
				}
				else if ((18 <= j && j <= 25) && (0 <= i && i <= 59)) {

					if ( (2 <= i && i <= 9) || (50 <= i && i <= 57)) {
						draw_pixel(i + x, j + y, GREY); 
					}
					else {
						draw_pixel(i + x, j + y, WHITE);
					}					
				}
			}
			else if (26 <= j && j <= 59) { // Front part

				// Guns
				if ((26 <= j && j <= 44) && ((4 <= i && i <= 6) || (54 <= i && i <= 56))) {
					draw_pixel(i + x, j + y, WHITE);	
				}
				
				// Middle part
				if (j == 26 && (15 <= i && i <= 45)) {
					draw_pixel(i + x, j + y, WHITE);
				}
				else if ( (27 <= j && j <= 42) && (24 <= i && i <= 36) ) {

					if ((28 <= j && j <= 40) && (26 <= i && i <= 34)) {

						if ((31 <= j && j <= 37) && (29 <= i && i <= 31)) {
							draw_pixel(i + x, j + y, BLUE);	// 0000 0|110 000|1 1000
						}
						else {
							draw_pixel(i + x, j + y, BLACK);
						}						
					} 
					else {
						draw_pixel(i + x, j + y, WHITE);
					}					
				}
				else if ((43 <= j && j <= 52) && (26 <= i && i <= 34)) {
					draw_pixel(i + x, j + y, WHITE);
				}
				else if ((53 <= j && j <= 59) && (28 <= i && i <= 32)) {
					draw_pixel(i + x, j + y, color);
				}
			}			
		}

		if (k >= 15) {
			increasing = false;
		}
		if (k <= 0) {
			increasing = true;
		}

		if (add) {
			if (increasing) {
				k++;
			}
			else {
				k--;
			}
		}
		//printf("3. Y:%i   K:%i\n", j, k);
	}
}




bool is_on_end(Enemy* enemy) {
	if (enemy->y > 280) return true;
	else return false;
}



int update_enemies(Enemy** enemies, int number_of_enemies, bool canmove) {
	int counter = 0;

	for (int i = 0; i < number_of_enemies; i++) {
		if (enemies[i] != NULL) {
			draw_enemy(enemies[i]->x, enemies[i]->y, enemies[i]->hp);
			counter++;

			if (canmove) {
				int dir = DOWN;
				move_en(enemies[i], dir);

				if (is_on_end(enemies[i])) {
					free(enemies[i]);
					enemies[i] = NULL;
					return -1;
				}
			}
		}
	}

	return counter;
}

void free_enemies(Enemy** enemies, int number_of_enemies) {

	for (int e = 0; e < number_of_enemies; e++) {
		if (enemies[e] != NULL) {
			free(enemies[e]);
			enemies[e] = NULL;
		}
	}
}

