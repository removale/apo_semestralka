#ifndef GAME_OVER_H
#define GAME_OVER_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>
#include <termios.h>

#include "sw.h"



void draw_game_over(unsigned char* parlcd_mem_base, int score);

void play_anim(unsigned char* parlcd_mem_base);


#endif  /*GAME_OVER_H*/