#ifndef SW_H
#define SW_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>
#include <termios.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

#include "enemy.h"
#include "shot.h"
#include "color_utils.h"
#include "player_utils.h"
#include "game_over.h"
#include "game.h"
#include "upgrade_package.h"

unsigned short* fb;
font_descriptor_t* fdes;

void draw_pixel(int x, int y, unsigned short color);

void draw_pixel_big(int x, int y, unsigned short color, int scale);

int char_width(int ch);

void draw_char(int x, int y, char ch, unsigned short color, int scale);

void draw_string(int x, int y, char* string, unsigned short color, int scale);

void main_menu(unsigned char* parlcd_mem_base);

int main(int argc, char* argv[]);


#endif  /*SW_H*/
