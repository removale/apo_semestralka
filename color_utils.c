#include "color_utils.h"


#ifndef RED_COLOR
#define RED_COLOR 0xf00000
#endif 

#ifndef GREEN_COLOR
#define GREEN_COLOR 0x00d000
#endif 

#ifndef LIGHT_BLUE_COLOR
#define LIGHT_BLUE_COLOR 0x00FFFF
#endif 

#ifndef BLUE_COLOR
#define BLUE_COLOR 0x0000FF
#endif 

#ifndef YELLOW_COLOR
#define YELLOW_COLOR 0xFCFF00
#endif 

#ifndef WHITE_COLOR
#define WHITE_COLOR 0xFFFFFF
#endif 

#ifndef RED_COLOR_HSV
#define RED_COLOR_HSV 0
#endif 

#ifndef GREEN_COLOR_HSV
#define GREEN_COLOR_HSV 120
#endif

#ifndef LIGHT_BLUE_COLOR_HSV
#define LIGHT_BLUE_COLOR_HSV 180
#endif

#ifndef BLUE_COLOR_HSV
#define BLUE_COLOR_HSV 240
#endif

#ifndef YELLOW_COLOR_HSV
#define YELLOW_COLOR_HSV 60
#endif

unsigned int hsv2rgb_lcd(int hue, int saturation, int value) {
	hue = (hue % 360);
	float f = ((hue % 60) / 60.0);
	int p = (value * (255 - saturation)) / 255;
	int q = (value * (255 - (saturation * f))) / 255;
	int t = (value * (255 - (saturation * (1.0 - f)))) / 255;
	unsigned int r, g, b;
	if (hue < 60) {
		r = value; g = t; b = p;
	}
	else if (hue < 120) {
		r = q; g = value; b = p;
	}
	else if (hue < 180) {
		r = p; g = value; b = t;
	}
	else if (hue < 240) {
		r = p; g = q; b = value;
	}
	else if (hue < 300) {
		r = t; g = p; b = value;
	}
	else {
		r = value; g = p; b = q;
	}
	r >>= 3;
	g >>= 2;
	b >>= 3;
	return (((r & 0x1f) << 11) | ((g & 0x3f) << 5) | (b & 0x1f));
}


unsigned int hex2color(uint32_t color) {
	switch (color) {
	case RED_COLOR:
		return RED_COLOR_HSV;
		break;
	case YELLOW_COLOR:
		return YELLOW_COLOR_HSV;
		break;
	case GREEN_COLOR:
		return GREEN_COLOR_HSV;
		break;
	case LIGHT_BLUE_COLOR:
		return LIGHT_BLUE_COLOR_HSV;
		break;
	case BLUE_COLOR:
		return BLUE_COLOR_HSV;
		break;
	default:
		return RED_COLOR_HSV;
		break;
	}
}

uint32_t color2hex(unsigned int color) {
	switch (color) {
	case RED_COLOR_HSV:
		return RED_COLOR;
		break;
	case YELLOW_COLOR_HSV:
		return YELLOW_COLOR;
		break;
	case GREEN_COLOR_HSV:
		return GREEN_COLOR;
		break;
	case LIGHT_BLUE_COLOR_HSV:
		return LIGHT_BLUE_COLOR;
		break;
	case BLUE_COLOR_HSV:
		return BLUE_COLOR;
		break;
	default:
		return WHITE_COLOR;
		break;
	}
}

unsigned int choose_color(unsigned int color) {

	bool choosing_color = true;
	char ch = ' ';
	int selector = 0;

	printf("Color is %i\n", color);
	printf("Color in hex is %x\n", color2hex(color));

	uint32_t choosen_color = color2hex(color);
	uint32_t actual_color = choosen_color;

	unsigned char* mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
	if (mem_base == NULL) {
		exit(1);
	}

	printf("Choosing color: START...\n");
	//struct timespec loop_delay = { .tv_sec = 0, .tv_nsec = 200 * 1000 * 1000 };

	while (choosing_color) {

		int r = read(0, &ch, 1);
		while (r == 1) {
			if (ch == 'w') {
				selector--;
				if (selector < 0) {
					selector = 4;
				}
			}
			else if (ch == 's') {
				selector++;
			}
			else if (ch == 'd') {
				choosen_color = actual_color;
			}
			else if (ch == 'a') {
				choosing_color = false;
				return hex2color(choosen_color);
			}
			r = read(0, &ch, 1);
		}

		selector = selector % 5;

		switch (selector) {
		case 0:
			actual_color = RED_COLOR;
			break;
		case 1:
			actual_color = GREEN_COLOR;
			break;
		case 2:
			actual_color = LIGHT_BLUE_COLOR;
			break;
		case 3:
			actual_color = BLUE_COLOR;
			break;
		case 4:
			actual_color = YELLOW_COLOR;
			break;
		default:
			break;
		}

		printf("Color selector: %i\n", selector);

		*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = choosen_color;
		*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = actual_color;
		sleep(1);
	}

	return choosen_color;
}