#include "upgrade_package.h"

#ifndef WIDTH
#define WIDTH 480 
#endif 

#ifndef HEIGTH
#define HEIGTH 320 
#endif   
 


void create_package(Package* package) {

	//Package* p = malloc(sizeof(Package));
	package->x = 100;
	package->y = 100;
	package->double_shot = true;
	package->size = 60;
	package->multx = 1;
	package->multy = 1;
	//package = p;	
}

void create_packages(Package** packages, int number_of_packages, bool double_shot) {

	for (int p = 0; p < number_of_packages; p++) {
		if (packages[p] == NULL) {
			Package* package = malloc(sizeof(Package));
			package->x = 150;
			package->y = 150;
			package->double_shot = true;
			package->size = 60;
			package->multx = 1;
			package->multy = 1;
			packages[p] = package;
			break;
		}
	}
}

void move_package(Package* package) {

	if ((package->x + 10 * package->multx) >= (WIDTH - package->size) || (package->x + 10 * package->multx) <= 0) {
		package->multx = package->multx * -1;
	}

	if ((package->y + 10 * package->multy) >= (HEIGTH - package->size) || (package->y + 10 * package->multy) <= 0) {
		package->multy = package->multy * -1;
	}

	package->x = (package->x + (10 * package->multx));
	package->y = (package->y + (10 * package->multy));

}

void draw_package(Package* package) {
	//printf("DRAWING SHOT...");

	for (int y = 0; y < package->size; y++) { // height 
		for (int x = 0; x < package->size; x++) { // width 

			if (20 <= y && y <= 40) {
				if ((20 <= x && x <= 24) || (35 <= x && x <= 39)) {
					draw_pixel(x + package->x, y + package->y, 0x0010);
				}
				else {
					draw_pixel(x + package->x, y + package->y, 0x0700);
				}
			}
			else {
				draw_pixel(x + package->x, y + package->y, 0x0700);
			}
		}
	}
}

bool package_taken(Package* package, int player_x, int player_y, int player_size) {
	bool taken = false;

	//printf("PACKAGE COMPARE");
	if (package != NULL) {
		// if package colide with player
		//printf("Coliding");
		if ( ( (player_x <= package->x) && (package->x <= player_x + player_size) && (player_y <= package->y) && ( package->y <= player_y + player_size) ) ||
			 ( (player_x <= package->x + package->size) && (package->x + package->size <= player_x + player_size) && (player_y <= package->y) && (package->y <= player_y + player_size) ) ||
			 ( (player_x <= package->x) && (package->x <= player_x + player_size) && (player_y <= package->y + package->size) && (package->y + package->size <= player_y + player_size) ) ||
			 ( (player_x <= package->x + package->size) && (package->x + package->size <= player_x + player_size) && (player_y <= package->y + package->size) && (package->y + package->size <= player_y + player_size)) ) {
			taken = true;
			free(package);
			package = NULL;
		}
	}
	

	return taken;
}


