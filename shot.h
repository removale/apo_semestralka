#ifndef SHOT_H
#define SHOT_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "enemy.h"
#include "sw.h"

typedef struct Shot {
    int x;
    int y;
}Shot;


void create_shot(Shot** shots, int numbers_of_shots, int x, int y, bool double_shot);

void move_shot(Shot* shot);

void draw_shot(int x, int y);

int shoted(Shot** shots, int numbers_of_shots, Enemy** enemies, int number_of_enemies);

bool is_shot_on_end(Shot* shot);

void update_shots(Shot** shots, int numbers_of_shots);


#endif  /*SHOT_H*/
