#ifndef COLOR_UTILS_H
#define COLOR_UTILS_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

unsigned int hsv2rgb_lcd(int hue, int saturation, int value);

unsigned int hex2color(uint32_t color);

uint32_t color2hex(unsigned int color);

unsigned int choose_color(unsigned int color);


#endif  /*ENEMY_H*/
