#include "game.h"

unsigned short* fb;
unsigned char* mem_base;

int ptr;
int player_x;
int player_y;
int player_score;
int max_enemy_count;
int max_shots_count;
int enemy_count;
int control;

bool double_shot;
bool do_once;

Enemy** enemies;
Shot** shots;
Package* package;


void game(unsigned char* parlcd_mem_base, unsigned int player_color) {
	
	player_x = 0;
	player_y = 240;
	player_score = 0;
	max_enemy_count = 100;
	max_shots_count = 20;
	srand(time(NULL));
	enemy_count = rand() % 3 + 1;

	double_shot = false;
	do_once = true;

	fb = (unsigned short*)malloc(320 * 480 * 2);	

	mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

	//alocating memory for all enemies, shots and package
	enemies = (Enemy**)malloc(max_enemy_count * sizeof(Enemy*));
	shots = (Shot**)malloc(max_shots_count * sizeof(Shot*));
	package = (Package*)malloc(sizeof(Package));

	//init enemies
	init_enemies(enemies, max_enemy_count, enemy_count,1);

	game_loop(parlcd_mem_base, player_color);

	draw_game_over(parlcd_mem_base, player_score);



	
}

void game_loop(unsigned char* parlcd_mem_base, unsigned int player_color) {

	char ch = ' ';
	int canmovecount = 0;
	bool canmove = true;
	bool play = true;
	int en_hp = 1;
	struct timespec loop_delay = { .tv_sec = 0, .tv_nsec = 200 * 1000 * 1000 };
	printf("Game: STARTED...\n");

	while (play) {

		for (ptr = 0; ptr < 320 * 480; ptr++) {
			fb[ptr] = 0;
		}

		// interaction from player
		int r = read(0, &ch, 1);
		while (r == 1) {
			if (ch == 'w') {
				if (player_y > 20) {
					player_y -= 20;
				}
			}
			else if (ch == 's') {
				if (player_y < 300 - 60) {
					player_y += 20;
				}
			}
			else if (ch == 'd') {
				if (player_x < 480 - 90) {
					player_x += 50;
				}
			}
			else if (ch == 'a') {
				if (player_x > 0) {
					player_x -= 50;
				}
			}
			else if (ch == 'e') {
				create_shot(shots, max_shots_count, player_x, player_y, double_shot);
			}
			r = read(0, &ch, 1);
		}

		//move enemies
		canmovecount++;

		//faster enemies with high score
		if (player_score < 5) { if (canmovecount % 4 == 0) { canmove = true; } else canmove = false; }
		else if (player_score < 25) { en_hp = 2; if (canmovecount % 3 == 0) { canmove = true; } else canmove = false; }
		else if (player_score > 24) { if (canmovecount % 2 == 0) { canmove = true; } else canmove = false; }

		// update enemies
		if ((control = update_enemies(enemies, max_enemy_count, canmove)) == -1) {
			play = false;
		}
		else if (control == 0) {
			init_enemies(enemies, max_enemy_count, enemy_count, en_hp);
		}


		// drawing player
		draw_player(player_x, player_y, player_color);

		// updating and drawing shots
		player_score += shoted(shots, max_shots_count, enemies, max_enemy_count);
		update_shots(shots, max_shots_count);

		//printing score
		*(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = (uint32_t)player_score;

		// Updating boost package
		if (player_score > 10) {

			if (do_once) {
				create_package(package);
				do_once = false;
			}

			if (!double_shot) {
				double_shot = package_taken(package, player_x, player_y, 60);
				move_package(package);
				draw_package(package);
			}
		}


		// updating level
		enemy_count = get_enemies_from_score(player_score);



		// showing on LCD
		parlcd_write_cmd(parlcd_mem_base, 0x2c);
		for (ptr = 0; ptr < 480 * 320; ptr++) {
			parlcd_write_data(parlcd_mem_base, fb[ptr]);
		}
		clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
	}

	free_enemies(enemies, max_enemy_count);
	free(enemies);
	free(shots);
	free(package);

}


