#ifndef ENEMY_H
#define ENEMY_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>


typedef struct Enemy {
    int x;
    int y;
    int hp;
}Enemy;


void init_enemies(Enemy** enemies, int number_of_enemies, int make_enemies, int hp);

void move_en(Enemy* enemy, int dir);

void draw_enemy(int x, int y, int hp);

bool is_on_end(Enemy* enemy);

int update_enemies(Enemy** enemies, int number_of_enemies, bool canmove);

void free_enemies(Enemy** enemies, int number_of_enemies);



#endif  /*ENEMY_H*/
