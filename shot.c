#include "shot.h"


void create_shot(Shot** shots, int numbers_of_shots, int x, int y, bool double_shot) {

	for (int s = 0; s < numbers_of_shots; s++) {
		if (shots[s] == NULL) {
			if (double_shot && (s+1) < numbers_of_shots) {
				Shot* shot1 = malloc(sizeof(Shot));
				Shot* shot2 = malloc(sizeof(Shot));

				shot1->x = x + 25;
				shot2->x = x + 35;

				shot1->y = y;
				shot2->y = y;

				shots[s] = shot1;
				shots[s + 1] = shot2;
				break;
			}
			else {
				Shot* shot = malloc(sizeof(Shot));
				shot->x = x + 30;
				shot->y = y;
				shots[s] = shot;
				break;
			}
		}
	}
}

void move_shot(Shot* shot) {
	shot->y -= 20;
}

void draw_shot(int x, int y) {
	//printf("DRAWING SHOT...");

	for (int j = 0; j < 20; j++) { // height oh shot
		for (int i = 0; i < 1; i++) { // width of shot
			// 0000 0|111 111|0 0000
			draw_pixel(i + x, j + y, 0x0770); // 0x07e0
		}
	}
}

int shoted(Shot** shots, int numbers_of_shots, Enemy** enemies, int number_of_enemies) {
	int shoted = 0;

	for (int s = 0; s < numbers_of_shots; s++) {
		for (int e = 0; e < number_of_enemies; e++) {

			if (shots[s] != NULL && enemies[e] != NULL) {
				// collision if shot shoted enemy
				if (((enemies[e]->x <= shots[s]->x) && (shots[s]->x <= (enemies[e]->x + 60))) &&
					(shots[s]->y <= (enemies[e]->y + 60))) {
					free(shots[s]);
					if(enemies[e]->hp ==1){
					free(enemies[e]);
					shoted++;
					enemies[e] = NULL;}
					else enemies[e]->hp --;
					shots[s] = NULL;
					
				}
			}
		}
	}

	return shoted;
}


bool is_shot_on_end(Shot* shot) {
	if (shot->y <= 0) {
		return true;
	}
	return false;
}

void update_shots(Shot** shots, int numbers_of_shots) {
	for (int s = 0; s < numbers_of_shots; s++) {
		if (shots[s] != NULL) {
			move_shot(shots[s]);
			draw_shot(shots[s]->x, shots[s]->y);
			if (is_shot_on_end(shots[s])) {
				free(shots[s]);
				shots[s] = NULL;
			}
		}
	}
}
