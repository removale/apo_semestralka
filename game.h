#ifndef GAME_H
#define GAME_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

#include "enemy.h"
#include "shot.h"
#include "color_utils.h"
#include "player_utils.h"
#include "game_over.h"
#include "upgrade_package.h"



void game(unsigned char* parlcd_mem_base, unsigned int player_color);

void game_loop(unsigned char* parlcd_mem_base, unsigned int player_color);

#endif  /*GAME_H*/

