#ifndef PACKAGE_H
#define PACKAGE_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "sw.h"

typedef struct Package {
    int x;
    int y;
    bool double_shot;
    int size;
    int multx;
    int multy;
}Package;

void create_package(Package* package);

void create_packages(Package** packages, int number_of_packages, bool double_shot);

void move_package(Package* package);

void draw_package(Package* package);

bool package_taken(Package* package, int player_x, int player_y, int player_size);


#endif  /*PACKAGE_H*/