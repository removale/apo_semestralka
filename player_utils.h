#ifndef PLAYER_UTILS_H
#define PLAYER_UTILS_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>



void draw_player(int x, int y, unsigned int color);

int get_enemies_from_score(int score);


#endif  /*PLAYER_UTILS_H*/